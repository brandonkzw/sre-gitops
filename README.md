# sre-gitops

## Requirements

Tested on Kubernetes `1.29`

## Getting started

1. K8s Connected - You must have the forked git repo connected to your Kubernetes cluster via Gitlab Agent [docs](https://docs.gitlab.com/ee/user/clusters/agent/index.html).
2. Gitlab CI/CD Variables - You must have configured your git repo's gitlab ci/cd variables with the following

| Required Gitlab CI/CD Variables | Example |
|---------------------------------| ------- |
| ELASTICSEARCH_HOST              | elasticsearch-master |
| ELASTICSEARCH_PORT              | 9200 |
| ELASTICSEARCH_USERNAME          | elasticusername |
| ELASTICSEARCH_PASSWORD          | elasticpassword |
| APM_ENDPOINT                    | <https://apmserver.elastic.com:9243> |
| APM_BEARER_TOKEN                | 10nZVtbZKava7VUsNTe1IP8BBx/Ze6fnXaEa1Y1HJnBkk2YbP0aiHFmBtH6Jd-VB |
| K8S_AGENT_CONTEXT               | brandonkzw/sre-gitops:k8s-agent |

## How does this work?

This repository contains a GitLab CI/CD pipeline for managing the installation and upgrade of various Kubernetes components. The pipeline is divided into two stages: check_kubernetes_access and install-or-upgrade. Each stage consists of jobs that handle specific tasks related to the deployment and management of Kubernetes resources.

## Stages

### 1. check_kubernetes_access

- This stage is responsible for checking the access to the Kubernetes cluster.
  - It includes a job that verifies the Kubernetes cluster access and ensures that the necessary configurations are in place.

### 2. install-or-upgrade

- This stage focuses on installing or upgrading various Kubernetes components and tools.
- It includes multiple jobs, each dedicated to installing or upgrading a specific component.

## Jobs

### certmanager

- This job checks if cert-manager is installed and applies the necessary configurations.
- It installs or upgrades the cert-manager Helm chart based on the specified version.

### fluentbit

- This job is responsible for patching Fluentbit configurations and installing or upgrading the Fluentbit Helm chart.

### metricbeats

- This job checks if the required credentials are available and installs or upgrades metricbeats.
- It also patches the configuration with Elasticsearch credentials obtained from GitLab CI/CD Variables.

### opentelemetry-operator

- This job installs or upgrades the opentelemetry-operator Helm chart in the designated namespace.

### opentelemetry-collector

- This job installs or upgrades the opentelemetry-collector Helm chart in the designated namespace, setting the mode to daemonset.

Each job in the `install-or-upgrade` stage is set to run manually, allowing for control over when these operations are performed. The pipeline is designed to ensure that the necessary tools and components are properly installed and configured within the Kubernetes environment.
