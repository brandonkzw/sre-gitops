#! /bin/sh
echo "Patching Fluentbit Filters"
filters=$(yq e '.config.filters' filters.yaml)
yq e ".config.filters = \"$filters\"" -i values-template.yaml

echo "Patching Fluentbit Inputs"
inputs=$(yq e '.config.inputs' inputs.yaml)
yq e ".config.inputs = \"$inputs\"" -i values-template.yaml

echo "Patching Fluentbit outputs"
outputs=$(yq e '.config.outputs' outputs.yaml)
yq e ".config.outputs = \"$outputs\"" -i values-template.yaml

echo "Patching Fluentbit customParsers"
customparser=$(yq e '.config.customParsers' customParsers.yaml)
yq e ".config.customParsers = \"$customparser\"" -i values-template.yaml