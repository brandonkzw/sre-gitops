stages:
  - check_kubernetes_access
  - configure
  - install-or-upgrade

variables:
  CERTMANAGER_VERSION: v1.14.2
  FLUENTBIT_VERSION: 0.43.0
  METRICBEAT_VERSION: 8.11.4
  OPENTELEMETRY_OPERATOR_VERSION: 0.47.0
  OPENTELEMETRY_COLLECTOR_VERSION: 0.81.2

  KUBECTL_BINARY_URL: https://dl.k8s.io/release/v1.29.2/bin/linux/amd64/kubectl
  HELM_BINARY_URL: https://get.helm.sh/helm-v3.14.1-linux-amd64.tar.gz
  YQ_BINARY_URL: https://github.com/mikefarah/yq/releases/download/v4.41.1/yq_linux_amd64

  # Gitlab CI/CD Variables, remember to set the VALUE of the following in your gitlab ci/cd variables
  ES_HOST: $ELASTICSEARCH_HOST
  ES_PORT: $ELASTICSEARCH_PORT
  ES_USERNAME: $ELASTICSEARCH_USERNAME
  ES_PASSWORD: $ELASTICSEARCH_PASSWORD
  KUBERNETES_AGENT_CONTEXT: $K8S_AGENT_CONTEXT
  APM_TOKEN: $APM_BEARER_TOKEN
  APM_EP: $APM_ENDPOINT
  

.install_kubectl_helm:
  before_script:
    - apk add curl
    - curl -LO ${KUBECTL_BINARY_URL} && chmod +x kubectl && mv kubectl /usr/local/bin/kubectl
    - curl -s ${HELM_BINARY_URL} | tar -zxvf - && chmod +x linux-amd64/helm && mv linux-amd64/helm /usr/local/bin/helm
    - curl -LO ${YQ_BINARY_URL} && chmod +x yq_linux_amd64 && mv yq_linux_amd64 /usr/local/bin/yq
    - kubectl config use-context $K8S_AGENT_CONTEXT

check_kubernetes_access:
  stage: check_kubernetes_access
  image: alpine:3.19.1
  extends: .install_kubectl_helm
  script:
    - echo "Checking Kubernetes cluster access..."
    - kubectl config get-contexts
    - if ! kubectl cluster-info; then exit 1; else echo "Kubernetes Access OK"; fi

certmanager:
  stage: install-or-upgrade
  needs: ["check_kubernetes_access"]
  image: alpine:3.19.1
  extends: .install_kubectl_helm
  script:
    - echo "Checking if cert-manager is installed..."
    - if [ "$(helm list -A | grep cert-manager)" == "" ]; then export CERTMANAGER_INSTALLED=FALSE; else export CERTMANAGER_INSTALLED=TRUE; fi
    - echo "Applying kubectl configurations..."
    - if [ $CERTMANAGER_INSTALLED == "TRUE" ]; then kubectl apply -f cert-manager.crds-${CERTMANAGER_VERSION}.yaml; else echo "Certmanager was installed, skipping CRD"; fi
    - echo "Installing/Upgrading helm charts..."
    - helm upgrade --install cert-manager --namespace cert-manager --create-namespace -f cert-manager/values.yaml ./charts/cert-manager-${CERTMANAGER_VERSION}.tgz  # Replace with the actual name of your Helm chart and directory
  when: manual
  
fluentbit:
  stage: install-or-upgrade
  needs: ["check_kubernetes_access"]
  image: alpine:3.19.1
  extends: .install_kubectl_helm
  script:
    - echo "[Fluentbit] Patching Fluentbit configurations"
    - cd fluentbit && chmod +x patch_fluentbit.sh
    - ./patch_fluentbit.sh && mv values-template.yaml values.yaml
    - cd ..
    - echo "Installing/Upgrading helm charts..."
    - helm upgrade --install fluent-bit --namespace fluentbit --create-namespace -f fluentbit/values.yaml ./charts/fluent-bit-${FLUENTBIT_VERSION}.tgz
  when: manual
  
metricbeats:
  stage: install-or-upgrade
  needs: ["check_kubernetes_access"]
  image: alpine:3.19.1
  extends: .install_kubectl_helm
  script:
    - echo "Checking if the ELASTICSEARCH_HOST ELASTICSEARCH_PORT ELASTICSEARCH_USERNAME ELASTICSEARCH_PASSWORD are initialized in Gitlab Variables"
    - if [ -z "$ELASTICSEARCH_HOST" ] || [ -z "$ELASTICSEARCH_PORT" ] || [ -z "$ELASTICSEARCH_USERNAME" ] || [ -z "$ELASTICSEARCH_PASSWORD" ]; then exit 1; else echo "Variables found, continuing.."; fi
    - echo "Installing/Upgrading metricbeats..."
    - kubectl apply -f metricbeats/metricbeat-kubernetes-${METRICBEAT_VERSION}.yaml
    - echo "Patching with Elasticsearch credential identified from Gitlab CI/CD Variables"
    - sed -i 's/ES_HOST_ChangeMeFromGitlabCIVariable/${ELASTICSEARCH_HOST}/g' metricbeats/values-daemonset.yaml
    - sed -i 's/ES_PORT_ChangeMeFromGitlabCIVariable/${ELASTICSEARCH_PORT}/g' metricbeats/values-daemonset.yaml
    - sed -i 's/ES_USERNAME_ChangeMeFromGitlabCIVariable/${ELASTICSEARCH_USERNAME}/g' metricbeats/values-daemonset.yaml
    - sed -i 's/ES_PASSWORD_ChangeMeFromGitlabCIVariable/${ELASTICSEARCH_PASSWORD}/g' metricbeats/values-daemonset.yaml
    - kubectl patch daemonset metricbeat -n kube-system --patch "$(cat metricbeats/values-daemonset.yaml)"
    - echo "Patching metricbeats.yaml"
    - kubectl patch configmap metricbeat-daemonset-config -n kube-system --patch "$(cat metricbeats/values-configmap.yaml)"
  when: manual
  
opentelemetry-operator:
  stage: install-or-upgrade
  needs: ["check_kubernetes_access"]
  image: alpine:3.19.1
  extends: .install_kubectl_helm
  script:
    - echo "Installing/Upgrading helm charts..."
    - helm upgrade --install opentelemetry-operator --namespace opentelemetry --create-namespace -f opentelemetry/values-operator.yaml ./charts/opentelemetry-operator-${OPENTELEMETRY_OPERATOR_VERSION}.tgz
    - echo "Pausing 30s for the pods to come up"
    - sleep 30
    - echo "Deploying default auto instrumentation"
    - kubectl apply -f opentelemetry/default-auto-instrumentation.yaml
  when: manual

opentelemetry-collector:
  stage: install-or-upgrade
  needs: ["check_kubernetes_access"]
  image: alpine:3.19.1
  extends: .install_kubectl_helm
  script:
    - echo "Checking if the APM_ENDPOINT and APM_BEARER_TOKEN are initialized in Gitlab Variables"
    - if [ -z "$APM_ENDPOINT" ] || [ -z "$APM_BEARER_TOKEN" ]; then exit 1; else echo "Variables found, continuing.."; fi
    - echo "Patching default collector endpoints"
    - sed -i 's/APM_ENDPOINT_DO_NOT_CHANGE_ME_USE_GITLAB_VARIABLE/${APM_ENDPOINT}/g' opentelemetry/values-collector.yaml
    - sed -i 's/APM_BEARER_TOKEN_DO_NOT_CHANGE_ME_USE_GITLAB_VARIABLE/${APM_BEARER_TOKEN}/g' opentelemetry/values-collector.yaml
    - echo "Installing/Upgrading helm charts..."
    - helm upgrade --install opentelemetry-collector --namespace opentelemetry --create-namespace -f opentelemetry/values-collector.yaml ./charts/opentelemetry-collector-${OPENTELEMETRY_COLLECTOR_VERSION}.tgz
  when: manual